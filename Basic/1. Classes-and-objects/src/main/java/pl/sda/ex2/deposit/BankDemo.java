package pl.sda.ex2.deposit;

import java.util.Scanner;

public class BankDemo {
    public static void main(String[] args) {

        BankDeposit num1 = new BankDeposit();

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj ilość środków na lokacie: ");
        num1.setTotalAmount(sc.nextDouble());
        System.out.println("Podaj czas trwania lokaty w dniach: ");
        num1.setDuration(sc.nextInt());

        System.out.println(num1.calculate());
    }

}
