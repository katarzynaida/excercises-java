package pl.sda.ex2.deposit;

import java.util.Scanner;

public class BankDeposit {

        private double totalAmount;
        private int duration;
        private double percentage = 0.03;

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }


    public double calculate() {
        return ((totalAmount * duration * percentage)/365)*0.19;
    }
}
