package pl.sda.ex1.rental;

public class CarRental {
    public static void main(String[] args) {

        Car car1 = new Car("Ford, ", "Fiesta", 5, "blue");
        Car car2 = new Car("Audi", "Q7", 3,"black");

        car1.display();
        car2.display();

    }
}
