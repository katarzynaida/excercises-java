package pl.sda.ex5.next.hospital;

import java.time.LocalDate;

public class Patient {
    private String firstname;
    private String lastname;
    private String pesel;
    private LocalDate birthDate;

    public Patient() {
    }

    public Patient(String firstname, String lastname, String pesel, LocalDate birthDate) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.pesel = pesel;
        this.birthDate = birthDate;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", pesel='" + pesel + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}