package pl.sda.ex5.next.hospital;

public class Hospital {
    private Patient[] patients;
    private int counter;
    private int maxNrOfPatients;

    public Hospital(int maxNrOfPatients) {
        this.maxNrOfPatients = maxNrOfPatients;
        patients = new Patient[maxNrOfPatients];
        counter = 0;
    }

    public void addPatient(Patient patient){
        if(counter < maxNrOfPatients) {
            patients[counter] = patient;
            counter++;
        } else {
            System.out.println("Przykro mi nie możemy Cię przyjąć");
        }
    }

    public void displayPatients(){
        for(Patient patient: patients){
            System.out.println(patient);
        }
    }
}
