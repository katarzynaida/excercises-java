package pl.sda.ex4.array;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;

public class arrayint {
    public static void main(String[] args) {
        //Utwórz 100 elementową tablicę typu int.
        // 2. Używając metody nextInt() z klasy Random oraz pętli for, wstaw do tablicy losowe liczby z
        // przedziału od 0 – 500.
        // 3. Używając algorytmu Bubble Sort, posortuj liczby w tablicy.

        int[] numbers = new int[500];

        Random random = new Random();

        for(int i = 0; i < numbers.length; i++){
            numbers[i] = random.nextInt(500);
        }
        for(int i = 0; i < numbers.length; i++) {
            boolean swap = false;

            for (int j = 0; j < numbers.length - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                    swap = true;
                }
                if (swap = false){
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(numbers));
    }
}
