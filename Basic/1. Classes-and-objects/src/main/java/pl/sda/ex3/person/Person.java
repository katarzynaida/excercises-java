package pl.sda.ex3.person;

public class Person {

    private String name;
    private int age;
    private String profession;

    public Person(){
    }

    public Person(String name, int age, String profession){
        this.name = name;
        this.age = age;
        this.profession = profession;
    }

    public int getAge() {
        return age;
    }
}
