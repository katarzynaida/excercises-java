package pl.sda.ex3.person;

import java.util.Random;

public class PersonDemo {

    public static int getAge() {
        Random random = new Random();
        int age = random.nextInt(100);
        return age;
    }

    public static void main(String[] args) {

        Person num1 = new Person("Magda", getAge(), "teacher");
        Person num2 = new Person("Ania", getAge(), "front-end developer");
        Person num3 = new Person("Iza", getAge(), "baker");
        Person num4 = new Person("Jagoda", getAge(), "dancer");
        Person num5 = new Person("Natalia", getAge(), "painter");

        Person[] people = new Person[5];
        people[0] = num1;
        people[1] = num2;
        people[2] = num3;
        people[3] = num4;
        people[4] = num5;

        System.out.println("Najłodsza osoba ma: " + findYoungestPerson(people).getAge() + " lat");
        System.out.println("Najstarsza osoba ma: " + findOldestPerson(people).getAge() + " lat");
        System.out.println("Wspólny wiek wszystkich osób wynosi: " + calculateTotalAge(people) + " lat");
        System.out.println("Średnia wieku wszystkich osób wynosi: " + calculateAverageAge(people) + " lata");
    }

    private static Person findYoungestPerson(Person[] people){
        int age1 = 100;
        int youngest = 0;
        for(int i = 0; i < people.length; i++)
            if(people[i].getAge() < age1){
                age1 = people[i].getAge();
                youngest = i;
            }
            return people[youngest];
    }

    private static Person findOldestPerson(Person[] people){
        int age1 = 100;
        int oldest = 0;
        for(int i = 0; i < people.length; i++)
            if(people[i].getAge() > age1){
                age1 = people[i].getAge();
                oldest = i;
            }
        return people[oldest];
    }

    private static double calculateTotalAge(Person[] people){
        double sumOfAge = 0;
        for(int i = 0; i < people.length; i++ ){
            sumOfAge = sumOfAge + people[i].getAge();
        }
        return sumOfAge;
    }

    private static double calculateAverageAge(Person[] people) {
        double averageOfAge = 0;
        for(int i = 0; i < people.length; i++ ){
            averageOfAge = calculateTotalAge(people) / people.length;
        }
        return averageOfAge;
    }
}
