package exloops;

import java.util.Scanner;

public class LoopsEx2 {
    public static void main(String[] args) {
        /*Napisz program który będzie wczytywał kolejne liczby tak długo aż użytkownik nie poda liczby 1000
        Po zakończeniu wczytywania, program powinien wypisać:
        ilość liczb ujemnych
        ilość liczb należących do przedziału <-100, 100>
        sumę wszystkich liczb
        średnią arytmetyczną */

        Scanner scanner = new Scanner(System.in);
        int sum = 0;
        int counter = 0; // ile liczb należy do danego przedziału
        int lessThanZero = 0;
        int ave = 0;

        int n = scanner.nextInt();

        while (!(n == 1000)) {
            if (n < 0) {
                lessThanZero++;
            }
            sum = sum + n;
            if (n >= -100 && n <= 100) {
                counter++;
            }
            ave++;
            n = scanner.nextInt();
        }

        System.out.println("Suma: " + sum);
        System.out.println("Ilość liczb należącyh do przedziału <-100; 100> : " + counter);
        System.out.println("Ilość liczb należącyh do przedziału mniejszych od zera: " + lessThanZero);

        if (ave > 0) {
            System.out.println("średnia: " + (double) sum / ave);
        }
    }
}

