package exloops;

import java.util.Scanner;

public class Loops {
    public static void main(String[] args) {
        /* Używając pętli while
a) Wypisać liczby od 102, 104, .. 200
b) Napisać program który wczyta a i b naturalne (wypisze odpowiedni komunikato gdy wartości są niepoprawne) oraz wypisze kolejne liczby od a do b ze zwiększaniem o 1
Np. Jak ktoś poda z klawiatury a = 3, b = 6, to program powinien wypisać 3,4,5,6 */

        //a)
        int a = 102;

        while(a <= 200){
            System.out.println(a);
            a+=2;

        }

        //b)
        System.out.println("Wprowadź liczbę a: ");
        Scanner reader = new Scanner(System.in);
        double num1 = reader.nextDouble();

        System.out.println("Wprowadź liczbę b: ");
        double num2 = reader.nextDouble();

        if(num1<0 || num2<0) {
            System.out.println("Wartość jest niepoprawna");
            return;
        }

        while(num1 != num2)   {
            System.out.println(num1);
            num1+=1;
        }

        //Zadanie: while, dokończyć. Obliczenie sumy 1 + ½ + ⅓ + … 1/n

        System.out.println("Wprowadź liczbę n, dla której policzy sumę 1 + 1/2 + 1/3 + ... + 1/n");
        double result = 0;
        int n = reader.nextInt();
        int k = 1; // zmienna kontrolna, kolejna wartość mianownika
        while(k <= n) {
            result = result + (double)1/k;
            k++;
        }
        System.out.println(result);
    }
}
