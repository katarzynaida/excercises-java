package exloops;

import java.util.Scanner;

public class LoopsEx3 {
    public static void main(String[] args) {

//        Przykład
//        Wczytujemy tak długo liczby całkowite, aż użytkowik nie poda liczby ujemnej
//      Na końcu należy wyśwetlić sumę wczytanych liczb  (bez ujemnej)

        Scanner scanner = new Scanner(System.in);

        int sum = 0;
        while (true) {
            int n = scanner.nextInt();
            if (n < 0) {
                break; // powoduje przerwanie pętli (ale nie funkcji jak return)
            }
            sum = sum + n;
        }
        System.out.println(sum);
    }
}
