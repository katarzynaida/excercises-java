package exloops;

import java.util.Scanner;

public class LoopsFor {
    public static void main(String[] args) {
        /* Wczytaj od użytkownika n całkowite a następnie, używając pętli for:
        a) Wypisz w pionie n symboli #  (każdy w osobnej linii)
        */

        System.out.println("Podaj liczbę n: ");
        Scanner reader = new Scanner(System.in);
        int n = reader.nextInt(); // ilość liczb do wczytania dla b i c

        for (int k = 1; k <= n; k++) {
            System.out.println("#");
        }

        //b) Wypisz w poziomie ciąg: 1010…. o długości n bitów np. dla n=3 ma być 101

        for (int i = 1; i <= n; i++) {
            if (i % 2 == 0) {
                System.out.println(0);
            } else {
                System.out.println(1);
            }
        }

        // c) Wszystkie liczby od 1 do n oraz informację czy dana liczba jest parzysta.
        //Np 1: nieparzysta, 2: parzysta, 3: nieparzysta …..

        for (int i = 1; i <= n; i++) {
            if (i % 2 == 0) {
                System.out.println(i + " parzysta");
            } else {
                System.out.println(i + " nieparzysta");
            }
        }
        //Zadanie * Napisz program który wczytuje n, a następnie wczytuje kolejne
        // n napisów. Program powinien wypisać na końcu całkowitą liczbę znaków wszystkich napisów.

        String word = new String("niedziela");

        for (int i = 1; i <= n; i++) {
            System.out.println(word);
        }
        System.out.println(n * word.length());
    }
}
