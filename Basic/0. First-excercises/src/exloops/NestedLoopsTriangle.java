package exloops;

import java.util.Scanner;

public class NestedLoopsTriangle {
    public static void main(String[] args) {
        /*
        Zadanie:
        a) dokończenie - używają  pętli zagnieżdżonej -  drukowanie trójkąta o zadanej
        wysokości n (wczytanej z klawiatury)
           1
           1 2
           1 2 3
           ….
           1 2 3  ...n
         */
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); //wysokość trójkąta to n,
        // ale to tez liczba licbz u podstawy, w ostatnim wierszu

        for(int i = 1; i <= n; i++){
            for(int k = 1; k <= i; k++){
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();

            /*
           b) * Napisać program który wczytuje liczbę n - wysokość trójkąta, a następnie
            wydrukuje trójkąt prostokątny o wysokości n następującej postaci
            Np. dla n = 4

                  *
                **
              ***
            **** (
             */

             for(int i = 1; i <= n; i++){
                // generujemy ity wiersz
                //wypisac odpowiednia ilosc spacji
                for(int j = 1; j <= n - i; j++){
                    System.out.print(" ");
                }
                //wypisać właściwą liczbę * (ta pętla wypisuje w poziomie)
                 for(int k = 1; k <= i; k++){
                     System.out.print('*');
                 }
                 System.out.println();
            }
    }
}
