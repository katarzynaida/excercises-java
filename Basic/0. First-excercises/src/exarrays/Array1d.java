package exarrays;

import java.util.Scanner;

public class Array1d {
    public static void main(String[] args) {
        /* Napisz program, który wczytuje n liczb rzeczywistych do tablicy jednowymiarowej
        a następnie zamienia na 0 wszystkie elementy ujemne
         */

        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        int[] numbers = new int[num];

        for (int i = 0; i < num; i++){
            numbers[i] = scanner.nextInt();
        }

        //zamiana na 0 elementy ujemne
        for(int i = 0; i < num; i++){
            if(numbers[i] < 0){
                numbers[i] = 0;
            }
        }
    }
}
