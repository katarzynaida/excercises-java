package exarrays;

import java.util.Arrays;
import java.util.Scanner;

public class Array1dpart2 {
    public static void main(String[] args) {
        //wczytac n napisow do tablicy i je odwrocic
        Scanner sc = new Scanner(System.in);
        int numOfWords = sc.nextInt();
        String[] words = new String[numOfWords];
        // wczytujemy kolejne napisy
        for(int i = 0; i < numOfWords; i++){
            words[i] = sc.next();
        }
        // ala, kicia, test -> test, kicia, ala
        //odwracamy tablice
        int i = 0, j = numOfWords - 1;
        while(i <= j){
            //zamiana miejscami elementu i-tego z j-tym
            // string pomocniczy
            String temp = words[i];
            words[i] = words[j];
            words[j] = temp;
            i++;
            j--;
        }
        System.out.println(Arrays.toString(words));
    }
}
