package homeex;

import java.util.Scanner;

public class Triangle {
    public static void main(String[] args) {
        /*Napisz program, który wczytuje n, pewien symbol (char)  a następnie wypisuje na ekranie
        “odwrócony”  trójkąt prostokątny Np. dla n = 4, oraz podanego znaku * program powinien wypisać:

                ****
                ***
                **
                *
        */
        System.out.println("Wprowadź długość boku trójkąta prostokątnego: ");

        Scanner reader = new Scanner(System.in);
        int numOfElements = reader.nextInt();

        System.out.println("Wprowadź znak, z którego zostanie utworzony odwrócony trójkąt prostokątny");
        char sign = reader.next().charAt(0);

        for (int i = 1; i <= numOfElements; numOfElements--) {
            for (int k = 1; k < numOfElements; k++) {
                System.out.print(sign);
            }
            System.out.println(sign);
        }
    }
}
