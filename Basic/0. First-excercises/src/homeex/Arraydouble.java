package homeex;

import java.util.Scanner;
import java.util.stream.DoubleStream;

public class Arraydouble {
    public static void main(String[] args) {
        /* Napisz program, który wczytuje n, następnie inicjalizuje tablicę n elementową liczb typu
        double, następnie wypełnia ją  n kolejnymi wartościami rzeczywistymi

        Następnie, używając kolejnej pętli for oblicz wyniki:
        ilość liczb ujemnych
        ilość liczb dodatnich
        sumę elementów dodatnich
        */
        System.out.println("Podaj n - liczba, z której ilości będzie składała się tablica");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        System.out.println("Wypisz pokolei wartości n liczb");

        double[] nelements = new double[n];

        for (int i = 0; i < n; i++) {
            nelements[i] = sc.nextDouble();
        }

        System.out.println("Wartości elementów ujemnych to: ");
        int counterless = 0;
        for (int i = 0; i < n; i++) {
            if (nelements[i] < 0) {
                System.out.println(nelements[i]);
                counterless++;
            }
        }
        System.out.println("Ilość elementów ujemnych to: " + counterless);


        System.out.println("Wartości elementów dodatnich to: ");
        int countermore = 0;
        double sum = 0;
        for (int i = 0; i < n; i++) {
            if (nelements[i] > 0) {
                System.out.println(nelements[i]);
                countermore++;
                sum = sum + nelements[i];
            }
        }
        System.out.println("Ilość elementów dodatnich to: " + countermore);
        System.out.println("Suma elementów dodatnich wynosi: " + sum);
    }
}
