package exconditions;

import java.util.Scanner;

public class IfEx {
    public static void main(String[] args) {
        //Wczytaj od użytkownika, do zmiennych  dwie liczby całkowite oraz wypisz na ekranie największa z nich

        Scanner reader = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę");
        int num1 = reader.nextInt();

        System.out.println("Podaj drugą liczbę");
        int num2 = reader.nextInt();

        if(num1 < num2) {
            System.out.println(num2);
        }else {
            System.out.println(num1);
        }

    }
}
