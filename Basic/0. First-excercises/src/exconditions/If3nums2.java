package exconditions;

import java.util.Scanner;

public class If3nums2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int num1 = sc.nextInt();
        int num2 = sc.nextInt();
        int num3 = sc.nextInt();
        //n1 n2 n3
        // 4 6 5

        if(num1 > num2) {
            if(num1 > num3) {
                System.out.println("Wynik: " + num1);
            }else {
                System.out.println("Wynik: " + num3);
            }
        }else if(num2 > num3) {
            System.out.println("Wynik: " + num2);
        }else {
            System.out.println("Wynik: " + num3);
        }
    }
}
