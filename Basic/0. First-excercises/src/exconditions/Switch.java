package exconditions;

import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
        //menu programu

        Scanner scanner = new Scanner(System.in);
        System.out.println("ofl - gra offline onl - online op-opcje wyj=wyjscie");
        String choice = scanner.nextLine();

        /* if(choice.equals("ofl") == true) {
            System.out.println("gra offline");
        } else if(choice.equals("onl") == true) {
            System.out.println("gra online");
        } else if(choice.equals("op") == true) {
            System.out.println("opcje");
        } else if(choice.equals("wyj") == true) {
            System.out.println("wyjscie");
        } else {
            System.out.println("Wybrałęś złą opcję");
        } */

        // mozna to samo zaimplementować inaczej:
        switch(choice) {
            case "ofl":
                System.out.println("gra offline");
                break;
            case "onl":
                System.out.println("gra online");
                break;
            case "op":
                System.out.println("opcje");
                break;
            case "wyj":
                System.out.println("wyjscie");
                break;
            default:
                System.out.println("Wybrałeś złą opcje");
        }
    }
}
