package exconditions;

import java.util.Scanner;

public class If {
    public static void main(String[] args) {
        boolean q = !true;
        int x = -1;
        if(q) {
            System.out.println("q jest prawdziwe");
        }else {
            System.out.println("q jest fałszywe");
        }

        Scanner scanner = new Scanner(System.in);
        double num = scanner.nextDouble();
        if(num < 0) {
            System.out.println("Podałeś liczbę ujemną");
        } else if (num == 0) {
            System.out.println("Podałeś liczbę zero");
        } else {
            System.out.println("Podałeś liczbę dodatnią");
        }

    }
}


