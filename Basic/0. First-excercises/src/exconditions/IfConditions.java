package exconditions;

public class IfConditions {
    public static void main(String[] args) {
        if(false || true) {
            System.out.println("Piwo");
        }

        if (1 > 2) {
            System.out.println("Niesporczak");
        } else if(!(true || 1 <2)) {
            System.out.println("Parasol");
        } else {
            System.out.println("Palindrom");
        }

        if(100 > 0) {
            if(!true) {
                System.out.println("Rambo");
            }
            if(false || false || true) {
                System.out.println("Moskit");
            }
        }
    }
}
