package exconditions;

import java.util.Scanner;

public class SwitchDivisionOn7 {
    public static void main(String[] args) {
        System.out.println("Menu:\ndiv7\nsum\nmult\ndiv");
        Scanner scanner = new Scanner(System.in);
        String option = scanner.next();
        if(!(option.equals("div7") || option.equals("sum") || option.equals("mult") || option.equals("div"))) {
            System.out.println("Zła opcja");
            return; //przerywa
        }
        double num1 = scanner.nextDouble();

        if(option.equals("div7")) {
            if(num1 % 7 == 0) {
                System.out.println("Podana liczba jest podzielna przez 7");
            }else {
                System.out.println("Podana liczba nie jest podzielna przez 7");
            }
            return; // przerwie działanie funkcji, czyli programu
        }
        double num2 = scanner.nextDouble();

        switch(option) {
            case "sum":
                System.out.println("wynik: " + (num1 + num2));
                break;
            case "mult":
                System.out.println("wynik: " + (num1 * num2));
                break;
            case "div":
                if (num2 == 0) {
                    System.out.println("Nie dziel przez 0");
                    return;
                }
                System.out.println("wynik: " + (num1 / num2));
                break;
            default:
                System.out.println("Wybrałeś niedozwoloną opcje");
        }
    }
}
