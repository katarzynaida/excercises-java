package exconditions;

import java.util.Scanner;

public class SwitchExerc {
    public static void main(String[] args) {
        //Napisz program (można użyć switch case) który wczytuje od użytkownika dwie liczby double, następnie jedna z trzech opcji: “sum”, “mult”, “div”
        //“sum” oznacza dodawanie - wówczas program powinien wypisać sumę wczytanych liczb
        //“mult” - iloczyn
        //“div” - iloraz - zwróć uwagę że nie można dzielić przez 0 - w przypadku gdy ktoś poda dzielnik równy 0, wyświetl odpowiedni komunikat.
        //Wyświetl również odpowiedni komunikat gdy ktoś wybierze niedozwoloną opcję.

        Scanner scanner = new Scanner(System.in);
        double n1 = scanner.nextDouble();
        double n2 = scanner.nextDouble();

        System.out.println("sum oznacza dodawanie, mult - iloczyn, a div - iloraz ");
        String choice = scanner.next();

        switch(choice) {
            case "sum":
                System.out.println(n1 + n2);
                break;
            case "mult":
                System.out.println(n1 * n2);
                break;
            case "div":
                if(n2 == 0) {
                    System.out.println("Nie dziel przez 0");
                }else {
                    System.out.println(n1 / n2);
                }
                break;
            default:
                System.out.println("Wybrałeś niedozwoloną opcje");
        }
    }
}
