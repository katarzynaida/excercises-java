package loopsfor;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //Zadanie: for, dokończyć. Obliczenie sumy 1 + ½ + ⅓ + … 1/n
        Scanner scanner = new Scanner(System.in);
        double result = 0;
        int n = scanner.nextInt();

        for (int k = 1; k <= n; k++) {
            result = result + 1.0 / k;
        }
        System.out.println(result);

        //for, wypisanie kolejnych liczb parzystych od 4 do 200

        for (int i = 4; i <= 200; i += 2) {
            System.out.println(i);
        }
        // kolejny przykład
        int v = 0;
        int z = 0;

        for (int i = 999; i > 0; i--) {
            if (i % 2 == 0 && i < 500) {
                v += 1; //suma liczba parzystych mniejszych od 500 i większych od 0
            }
            z += 1; // suma wszystkich podanych liczb
        }

        // for(;;){} //pętla nieskończona
        String word = "test 123 adas ee";
        // chcemy zliczyzc ilosc wystapien litery e
        int count = 0;
        for (int i = 0; i <= word.length() - 1; i++) {
            if (word.charAt(i) == 'e') {
                count++;
            }
        }
        System.out.println(count);
    }
}
