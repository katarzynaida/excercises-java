package nestedloops;

public class NestedLoops {
    public static void main(String[] args) {
        final int A = 3, B = 2; //A-wysokość, B - podstawa
        /*      i
            ##  1
            ##  2
            ##  3
         */
        // dla każdego wiersza (a razy ma się to wykonać bo wysokośc to a)
        for (int i = 1; i <= A; i++) {
            // wypisywanie b znaków # - wypisywanie itego wiersza
            for (int j = 1; j <= B; j++) {
                System.out.print('#');
            }
            System.out.println();


        }
    }
}
