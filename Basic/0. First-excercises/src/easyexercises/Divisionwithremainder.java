package easyexercises;

public class Divisionwithremainder {
    public static void main(String[] args) {
        //1 sposób
        // int to rzutowanie, zamienia liczbę z przecinkiem na całkowitą
        // jeżeli zaznacza na szaro tzn. że coś jest niepotrzebne
        int value = (int)(200 + 55)/2;
        System.out.println(value);

        //2 sposób
        double value2 = (200.0 + 55.0)/2.0;
        System.out.println(value2);

        //3 sposób
        double value3 = (double)(200.0 + 55.0)/2.0;
        System.out.println(value3);

        // zmienna float -> float mozemy rzutować(kastować) na int'a, i mozemy int'a na floata też rzutować

        float x = -99.0001f;
        int v = (int)x;

        int a = 23;
        float b = (float)a;

        double val = (double)1/2 + 3;
        System.out.println(val);

        double val2 = 1/2 + 3;
        System.out.println(val2);


    }
}
