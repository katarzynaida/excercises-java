package easyexercises;

public class Variablechar {
    public static void main(String[] args) {

        // char do obsługi znaków i w '....',  a
        char c = '!';
        System.out.println(c);

        // zamienia wykrzyknik na 33, bo taką ma interpretację w ASCII
        char d = '!';
        System.out.println((int)d);
    }
}
