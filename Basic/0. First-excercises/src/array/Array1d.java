package array;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Tablica 1D.
 *
 * Program, który wczyta n napisów od użytkownika i wstawi je do kolejych komórek tablicy
 * i wypisanie tych napisów z tablicy, które są dłuższe niż 5
 *
 * @author Katarzyna Woźniak
 */
public class Array1d {
    public static void main(String[] args) {

        //program, który wczyta n napisów od użytkownika i wstawi je do kolejych komórek tablicy
        // i wypisanie tych napisów z tablicy, które są dłuższe niż 5


        // pytamy uzytkownika ile napisow trzeba zapamietac
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilość słów");
        int numOfWords = scanner.nextInt();
        String[] words = new String[numOfWords];

        //wczytywanie kolejnych napisów z klawiatury i wstawianie ich do kolejnych komórek tablicy
        for (int i = 0; i < numOfWords; i++){
            words[i] = scanner.next();
        }

        // różne operacje na tablicy napisów

        //wypisanie tych napisów z tablicy, które są dłuższe niż 5
        for(int i = 0; i < numOfWords; i++){
            if(words[i].length() > 5){
                System.out.println(words[i]);
            }
        }

        // wartość bezwględna z tablicy
        int [] nums = {-1, 0, 2, -100, 999};
        for(int i = 0; i < nums.length; i++) {
            if (nums[i] < 0) {
                nums[i] = -nums[i];
            }
        }
        //nums[i] = Math.abs(nums[i]); //funkcja zwraca wartosc bezwgledna

        // operacje
        //1 sposob wypisania tablicy
        for(int i = 0; i < nums.length; i++){
            System.out.println(nums[i]);
        }
        // 2 sposób wypisania tablicy
        System.out.println(Arrays.toString(nums)); //toString zamienia na reprezentacje tekstowa

        for(int el : nums){
            System.out.println(el + " ");
        }
    }
}
