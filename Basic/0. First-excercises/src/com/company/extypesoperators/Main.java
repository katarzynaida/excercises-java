package com.company.extypesoperators;

public class Main {

    // funkcja (metoda) uruchomieniowa, od której zaczyna się program (start)
    public static void main(String[] args) {

        // zmienna - fragment pamięci RAM

        // deklaracja zmiennej z przypisaniem wartości  int number = 2;

        // deklaracja zmiennej
        int number;
        int number2;
        //przypisanie wartości
        number = 150;
        number2 = 200;
        int result = number + number2;
        System.out.println("liczba 1 wynosi " + number2 + " liczba 2 wynosi " + number + " , a ich suma to " + result);


        // typy logiczne
        boolean q = true || false && true;
        System.out.println(q);

        boolean q2 = false && (true || false && true);
        System.out.println(q2);

        boolean p = true;
        boolean s = !p;

        boolean r = p && s || s && true; // optymalizacja
        // boolean r2 = s&&(p || true); // p można zlikwidować
        // boolean r2 = s&&true; // optymalizacja
        boolean r2 = s;
    }
}
