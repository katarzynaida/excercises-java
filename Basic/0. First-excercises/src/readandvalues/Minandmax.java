package readandvalues;

import java.util.Scanner;

public class Minandmax {
    public static void main(String[] args) {
        //Zadanie * Wczytaj n całkowite a następnie n kolejnych liczb rzeczywistych. Program
        // powinien wypisać największa i najmniejszą z tych liczb.

        Scanner reader = new Scanner(System.in);
        int n = reader.nextInt();
        double val = reader.nextDouble(); // pierwsza liczba ciągu
        double min = val; //kandydat na minimum, a na końcu wynik
        double max = val;


        // n-1 iteracji
        // poniżej jest wyszukiwanie liniowe min i max (liniowy algorytm)
        for (int i = 1; i <= n - 1; i++) {
            //czytamy kolejną liczbę
            val = reader.nextDouble();

            //sprawdzamy czy nowa wartosc jest mniejsza od min
            if (val < min) {
                min = val; //aktualizacja - mamy nowe, lepsze min
            }
            if (val > max) {
                max = val;
            }
        }
        System.out.println("Min= " + min);
        System.out.println("max= " + max);
    }
}
