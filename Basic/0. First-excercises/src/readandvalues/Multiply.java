package readandvalues;

import java.util.Scanner;

public class Multiply {
    public static void main(String[] args) {

        // Napisz program który wczytuje dwie liczby rzeczywiste i wypisuje ich iloczyn

        Scanner reader = new Scanner(System.in);

        System.out.println("Podaj liczbę pierwszą");
        int number = reader.nextInt();

        System.out.println("Podaj liczbę drugą");
        int number2 = reader.nextInt();

        System.out.println(number * number2);
    }
}
