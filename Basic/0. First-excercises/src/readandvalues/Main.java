package readandvalues;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner reader = new Scanner(System.in); //system in czyli stadardowe wejscie czyli z klawiatury
        System.out.println("Podaj imię: ");
        // imię podane z klawaitury zostanie podstawione (=)
        String name = reader.nextLine();

        System.out.println("Podaj wiek: ");

        int age = reader.nextInt();
        System.out.println("Nazywasz się " + name + "\n i masz " + age + " lat");

        // \n przejście do nowej linii
    }
}
