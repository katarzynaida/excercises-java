package readandvalues;

import java.util.Scanner;

public class MultiplyNNums {
    public static void main(String[] args) {

        //Napisz program który wczytuje liczbę całkowitą n a następnie wypisuje wynik sumy 1 + 2 + 3 + ... +n

        Scanner reader = new Scanner(System.in);

        System.out.println("Wpisz liczbę całkowitą n: ");
        int n = reader.nextInt();

        int result = (((1 + n)/2)*n);
        System.out.println(result);
    }
}
