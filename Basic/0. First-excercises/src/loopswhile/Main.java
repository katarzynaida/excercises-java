package loopswhile;

public class Main {
    public static void main(String[] args) {
        // zmienna
        int iter = 1; // zmienna kontrolna dzięki, której można kontrolować czy pętla ma wykonać jeszcze
        // swoje instrukcje
        // wartość k mówi też o iteracji (powtórzeniu cyklu)

        //do robienia stałych używamy final i nazywamy ją DUŻYMI LITERAMI
        final int MAX = 50;
        // od iter = 1 , aż do 50 wypisz "*"
        while (iter <= MAX) {
            System.out.print("*");
            iter = iter + 1; // = iter++ (inaczej mozna zapisac) - inkrementacja (zwiększenie)
        }

        // kolejne liczby 100, 98, ..., 2

        int k = 100;

        while (k >= 2) {
            System.out.println(k);
            //k = k - 2; albo
            k -= 2;

        }
    }
}
