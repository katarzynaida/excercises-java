public class WorkShop {
    public static void main(String[] args) {

        BacisTuning bacisTuning = new BacisTuning(200, 400, 1500);
        bacisTuning.increaseEnginePower();
        bacisTuning.reduceMass();

        System.out.println(bacisTuning);

        ExtraTuning extraTuning = new ExtraTuning(200,400,1500);
        extraTuning.increaseEnginePower();
        extraTuning.reduceMass();

        System.out.println(extraTuning);

    }
}
