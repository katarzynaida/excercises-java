public class BacisTuning {
    private double enginepower;
    private double torque;
    private double mass;

    public BacisTuning(double enginepower, double torque, double mass) {
        this.enginepower = enginepower;
        this.torque = torque;
        this.mass = mass;
    }

    public double getEnginepower() {
        return enginepower;
    }

    public void setEnginepower(double enginepower) {
        this.enginepower = enginepower;
    }

    public double getTorque() {
        return torque;
    }

    public void setTorque(double torque) {
        this.torque = torque;
    }

    public double getMass() {
        return mass;
    }

    public void setMass(double mass) {
        this.mass = mass;
    }

    public void increaseEnginePower(){
        setEnginepower(getEnginepower()+20);
    }
    public void increaseTorque(){
        setTorque(getTorque()+10);
    }
    public void reduceMass(){
        setMass(getMass()-50);
    }

    @Override
    public String toString() {
        return "BacisTuning{" +
                "enginepower=" + enginepower +
                ", torque=" + torque +
                ", mass=" + mass +
                '}';
    }
}
