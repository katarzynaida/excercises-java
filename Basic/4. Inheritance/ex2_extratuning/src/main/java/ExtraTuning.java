public class ExtraTuning extends BacisTuning{
    public ExtraTuning(double enginepower, double torque, double mass) {
        super(enginepower, torque, mass);
    }

    @Override
    public void increaseEnginePower() {
        super.increaseEnginePower();
        setEnginepower(getEnginepower()*1.2);
    }

    @Override
    public void increaseTorque() {
        super.increaseTorque();
        setTorque(getTorque()*1.2);
    }

    @Override
    public void reduceMass() {
        super.reduceMass();
        setMass(getMass()*0.9);
    }
}
