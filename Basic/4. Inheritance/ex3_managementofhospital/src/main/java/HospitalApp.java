public class HospitalApp {
    public static void main(String[] args) {

        Person doctor1 = new Doctor("Janusz", "Kowalski", 15000, 3000);
        Person nurse1 = new Nurse("Małgorzata", "Król", 3000, 60);
        Person nurse2 = new Nurse("Celina", "Blok", 2800, 50);

        Hospital hospital = new Hospital(5);
        hospital.add(doctor1);
        hospital.add(nurse1);
        hospital.add(nurse2);

        System.out.println(hospital);
    }
}
