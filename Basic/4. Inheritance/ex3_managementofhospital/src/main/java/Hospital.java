import java.util.Arrays;

public class Hospital {
    /* przechowuje informacje o pracownikach szpitala w tablicy typu Person[]. Powinna istnieć
metoda add() pozwalająca dodać nowego pracownika do tablicy pracowników
*/

    private Person[] workers;
    private int i = 0;

    public Hospital(int maxNbOfWorkers) {
        workers = new Person[maxNbOfWorkers];
    }

    public void add(Person worker) {
        workers[i] = worker;
        i++;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "workers=" + "\n" + Arrays.toString(workers) +
                '}';
    }
}
