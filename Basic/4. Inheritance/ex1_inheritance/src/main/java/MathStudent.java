public class MathStudent extends Student {
    private String theBestMathsFormula;

    public MathStudent(String firstName, String secondName, int age, String nameOfUniversity, String major, String theBestMathsFormula) {
        super(firstName, secondName, age, nameOfUniversity, major);
        this.theBestMathsFormula = theBestMathsFormula;
    }

    public String getTheBestMathsFormula() { return theBestMathsFormula; }

    public void setTheBestMathsFormula(String theBestMathsFormula) { this.theBestMathsFormula = theBestMathsFormula; }
}
