public class Student extends Person {
    private String nameOfUniversity;
    private String major;

    public Student(String firstName, String secondName, int age, String nameOfUniversity, String major) {
        super(firstName, secondName, age);
        this.nameOfUniversity = nameOfUniversity;
        this.major = major;
    }

    public String getNameOfUniversity() { return nameOfUniversity; }

    public void setNameOfUniversity(String nameOfUniversity) { this.nameOfUniversity = nameOfUniversity; }

    public String getMajor() { return major; }

    public void setMajor(String major) { this.major = major; }
}
