public class Employee extends Person {
    private String proffesion;
    private double salary;

    public Employee(String firstName, String secondName, int age, String proffesion, double salary) {
        super(firstName, secondName, age);
        this.proffesion = proffesion;
        this.salary = salary;
    }

    public String getProffesion() { return proffesion; }

    public void setProffesion(String proffesion) { this.proffesion = proffesion; }

    public double getSalary() { return salary; }

    public void setSalary(double salary) { this.salary = salary; }
}
