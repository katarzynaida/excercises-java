public class Engineer extends Employee {
    private String EngeenerOf;

    public Engineer(String firstName, String secondName, int age, String proffesion, double salary, String engeenerOf) {
        super(firstName, secondName, age, proffesion, salary);
        EngeenerOf = engeenerOf;
    }

    public String getEngeenerOf() { return EngeenerOf; }

    public void setEngeenerOf(String engeenerOf) { EngeenerOf = engeenerOf; }
}
