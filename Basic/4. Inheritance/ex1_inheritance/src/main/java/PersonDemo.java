import java.util.Arrays;

public class PersonDemo {
    public static void main(String[] args) {

        Person itStudent = new ITStudent("Magda", "kozioł", 21, "Politechnika Krakowska",
                "Informatyka", "Java");

        Person student = new Student("Piotrek", "Kowalski", 22, "Politechnika Rzeszowska",
                "Inżynieria środowiska");

        Person driver = new Driver("Michał", "Wiśniewski", 28, "driver", 3200,
                "Ford", 2010);

        Person mathStudent = new MathStudent("Ola", "Pięta", 23, "Uniwersytet Ekonomiczny",
                "Matematyka", "wzór skróconego mnożenia" );

        Person[] people = {itStudent, student, driver, mathStudent};

        for(int i = 0; i < people.length; i++){
            System.out.println(people[i].getFirstName() + " " + people[i].getSecondName() + " " + people[i].getAge());
        }

    }
}
