public class ITStudent extends Student {
    private String programmingLanguages;

    public ITStudent(String firstName, String secondName, int age, String nameOfUniversity, String major, String programmingLanguages) {
        super(firstName, secondName, age, nameOfUniversity, major);
        this.programmingLanguages = programmingLanguages;
    }

    public String getProgrammingLanguages() { return programmingLanguages; }

    public void setProgrammingLanguages(String programmingLanguages) { this.programmingLanguages = programmingLanguages; }
}
