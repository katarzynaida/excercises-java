public class Driver extends Employee{
    private String modelOfCar;
    private int yearOfProducingCar;

    public Driver(String firstName, String secondName, int age, String proffesion, double salary, String modelOfCar, int yearOfProducingCar) {
        super(firstName, secondName, age, proffesion, salary);
        this.modelOfCar = modelOfCar;
        this.yearOfProducingCar = yearOfProducingCar;
    }

    public String getModelOfCar() { return modelOfCar; }

    public void setModelOfCar(String modelOfCar) { this.modelOfCar = modelOfCar; }

    public int getYearOfProducingCar() { return yearOfProducingCar; }

    public void setYearOfProducingCar(int yearOfProducingCar) { this.yearOfProducingCar = yearOfProducingCar; }
}
