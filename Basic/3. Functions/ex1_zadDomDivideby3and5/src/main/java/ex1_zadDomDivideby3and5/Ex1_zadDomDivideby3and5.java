package ex1_zadDomDivideby3and5;

import java.util.Scanner;

public class Ex1_zadDomDivideby3and5 {
    public static void main(String[] args) {
        /*
        Napisz program, który w przypadku liczb podzielnych przez 3 wyświetli liczbę i słowo Fizz,
        podzielnych przez 5 wyświetli liczbę i słowo Buzz, a podzielnych zarówno przez 3 i 5 wyświetli
        liczbę i słowo FizzBuzz. Pozostałe liczby nie powinny być wyświetlane. Zakładamy, że pracujemy
        na liczbach z przedziału od 1 – 150.
         */

        System.out.println("Wprowadź ilość liczb, które chcesz wprowadzić");
        Scanner scanner = new Scanner(System.in);
        int numberOfElements = scanner.nextInt();
        int[] ints = new int[numberOfElements];

        addingToArray(scanner, numberOfElements, ints);
        dividedBy3or5orBoth(ints);

    }

    private static void addingToArray(Scanner scanner, int numberOfElements, int[] ints) {
        for (int i = 0; i < numberOfElements;){
            System.out.println("Wprowadź liczbę (liczba powinny zawierać się w zakresie od 1 do 150): ");
            int number = scanner.nextInt();
            if (number < 1 || number > 150){
                System.out.println("Wprowadziłeś złą liczbę. Spróbuj ponownie.");
            }else {
                ints[i] = number;
                i++;
            }
        }
    }

    public static void dividedBy3or5orBoth (int[] ints){
        for (int element : ints){
            if (element % 3 == 0 && element % 5 == 0){
                System.out.println(element + "FizzBuzz");
            }else if (element % 3 == 0){
                System.out.println(element + "Fizz");
            }else if(element % 5 == 0){
                System.out.println(element + "Buzz");
            }
        }
    }
}
