public class Excercise3 {
    public static void main(String[] args) {
        /*
        Napisz funkcję sumującą wszystkie liczby całkowite w przedziale (a,b)
        public int sum(int a, int b)
         */
        int min = 2;
        int max = 10;
        System.out.println("Suma wszystkich elementów w przedziale <2;6> wynosi: " + sum(min, max));
    }
    public static int sum (int min, int max){
        int sum = 0;
        for(int i = min; i <= max; i++){
            sum = sum + i;
        }
        return sum;
    }
}
