import java.util.Scanner;

public class Excercise1 {
    public static void main(String[] args) {
        /*
        Napisz funkcję zwracającą wartość absolutną podanej liczby.
        public int abs(int a)
         */
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę, z której zostanie policzona wartośc bezwględna");
        double a = scanner.nextDouble();
        System.out.println("Wartość bezwględna wynosi: " + abs(a));
    }

    public static double abs(double a){
        double result = 0;
        if (a < 0){
            result = a*(-1);
        }
        if (a >= 0){
            result = a;
        }
        return result;
    }
}
