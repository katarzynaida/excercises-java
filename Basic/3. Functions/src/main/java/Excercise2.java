
public class Excercise2 {
    public static void main(String[] args) {
        /*
        Napisz funkcję znajdującą minimum w tablicy liczb całkowitych.
        public int min(int[] arr)
         */
        int[] arr = {-5, 0, 7, 14, -25, 48, 2, -14};
        System.out.println("Wartość minimalna w tablicy wynosi: " + min(arr));
    }

    public static int min(int[] arr){
        int minimum = arr[0];
        for(int i = 1; i < arr.length; i++){
            if(minimum > arr[i]){
                minimum = arr[i];
            }
        }
        return minimum;
    }
}
