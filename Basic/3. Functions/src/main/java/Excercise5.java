public class Excercise5 {
    public static void main(String[] args) {
        /*
        Napisz funkcję odwracającą String, np.: dla wejścia "robot" metoda zwróci "tobor"
        public String reverse(String str)
         */
        String word = "robot";
        System.out.println("Odwrócone słowo \"robot\" to: " + reverse(word) );
    }
    public static String reverse(String word){
        int lengthOfWord = word.length();
        String reverseWord = "";
        for(int i = lengthOfWord - 1; i >= 0; i--){
                reverseWord = reverseWord + word.charAt(i);
        }
        return reverseWord;
    }
}
