import java.util.Scanner;

public class Excersice5secondsSol {
    public static void main(String[] args) {
        /*
        Napisz funkcję odwracającą String, np.: dla wejścia "robot" metoda zwróci "tobor"
        Używając StringBuildera
         */
        System.out.println("Wpisz dowolny wyraz");
        System.out.println("Twój odwrócony wyraz brzmi: " + StringBuilder());
    }
    public static StringBuilder StringBuilder(){
        Scanner scanner = new Scanner(System.in);
        String word = scanner.next();
        StringBuilder reverseOfWord = new StringBuilder(word).reverse();
        return reverseOfWord;
    }
}
