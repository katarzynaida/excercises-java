import java.util.Scanner;

public class Excersice6 {
    public static void main(String[] args) {
        /*
        Napisz funkcję sprawdzającą czy dany String jest palindromem
        https://pl.wikipedia.org/wiki/Palindrom
        public boolean isPalindrome(String str)
         */
        System.out.println("Wprowadź słowo: ");
        System.out.println("Dane słowo to palindrom: " + isPalindrom());

    }
    public static boolean isPalindrom(){
        boolean isPalindrom = true;
        Scanner scanner = new Scanner(System.in);
        String str = scanner.next();
        int numberOfLetter = str.length();
        int min = 0;
        for(int i = numberOfLetter - 1; i > min; i--){
            if(str.charAt(min) == str.charAt(numberOfLetter - 1)){
                min++;
                numberOfLetter--;
            } else {
                isPalindrom = false;
            }
        }
        return isPalindrom;
    }
}
