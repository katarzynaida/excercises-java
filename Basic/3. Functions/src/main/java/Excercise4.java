public class Excercise4 {
    public static void main(String[] args) {
        /*
        Napisz podobną funkcję jak w 3. ale sumującą tylko parzyste liczby z podanego przedziału.
        public int sumEven(int a, int b)
         */
        int min = 2;
        int max = 10;
        System.out.println("Suma parzystych elementów w przedziale <2;6> wynosi: " + sumEven(min,max));
    }
        public static int sumEven(int min, int max){
            int sum = 0;
            for(int i = min; i <= max; i++){
                if(i%2 == 0){
                    sum = sum + i;
                }
            }
            return sum;
        }
}
