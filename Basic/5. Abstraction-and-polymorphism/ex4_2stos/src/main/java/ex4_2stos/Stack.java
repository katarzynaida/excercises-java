package ex4_2stos;

import java.util.Arrays;

/*
    Zaimplementuj strukturę danych stos jako klasę Stack z czterema metodami:
     public void push() – umieszcza na szczycie stosu nowy element
     public int top() – zwraca element znajdujący się na szczycie stosu, sam element pozostaje wciąż na stosie
     public int pop() – zwraca element znajdujący się na szczycie stosu, następnie go usuwa
     public boolean isEmpty() – zwraca true jeżeli stos nie zawiera żadnego elementu
    Użyj dynamicznej tablicy (w momencie gdy tablica jest pełna, tworzymy tablicę dwa razy większą i kopiujemy
    elementy, a gdy tablica jest wypełniona w mniej niż 50% to pomniejszamy ją dwa razy). Możesz do tego użyć
    metody System.arraycopy lub/i Arrays.copyOf.
     */
public class Stack {

    private static final int INITIAL_CAPACITY = 5;

    private int[] tab;
    private int index;
    private int numbersOfElements;

    public Stack(int size){
        tab = new int[size];
        index = -1;
        numbersOfElements = 0;
    }

    public Stack() {
        tab = new int[INITIAL_CAPACITY];
        index = -1;
        numbersOfElements = 0;
    }

    public void push(int element){
        if(numbersOfElements == tab.length){
            tab = Arrays.copyOf(tab, tab.length * 2);
        }
        tab[++index] = element;
        numbersOfElements++;
    }

    public int top(){
        return tab[index];
    }

    public int pop(){
        if(((double)numbersOfElements / tab.length) <= 0.5){
            tab = Arrays.copyOf(tab, tab.length / 2);
        }
        numbersOfElements--;
        return tab[index--];
    }

    public boolean isEmpty(){
        return numbersOfElements == 0;
    }

    public int length(){
        return tab.length;
    }
}
