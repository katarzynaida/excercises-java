package ex4_2stos;

public class StackDemo {
    public static void main(String[] args) {

        Stack stack = new Stack();

        System.out.println(stack.length());

        stack.push(5);
        stack.push(8);
        stack.push(15);
        stack.push(45);
        stack.push(25);
        stack.push(33);

        System.out.println(stack.length());

        System.out.println(stack.top());

        System.out.println(stack.pop());

        System.out.println(stack.top());

        System.out.println(stack.length());

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());

        System.out.println(stack.length());

    }
}
