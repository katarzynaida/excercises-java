package ex2_3Companypart1and2;

public class FullTimeEmployee extends Employee implements NetSalary {

    public FullTimeEmployee(String name, double grossSalary, String email) {
        super(name, grossSalary, email);
    }

    @Override
    public double calculateNetSalary() {
        return (getGrossSalary() - getFullTimeEmployeeSocialTax) - (getGrossSalary()*fullTimeEmployeeTax);
    }

    @Override
    public String toString() {
        return super.toString() + " " + "netSalary = " + calculateNetSalary();
    }
}
