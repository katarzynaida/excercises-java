package ex2_3Companypart1and2;

public abstract class Employee implements Person{
    private String name;
    private double grossSalary;
    private String email;

    public Employee(String name, double grossSalary, String email) {
        this.name = name;
        this.grossSalary = grossSalary;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGrossSalary() {
        return grossSalary;
    }

    public void setGrossSalary(double grossSalary) {
        this.grossSalary = grossSalary;
    }

    public  abstract double calculateNetSalary();

    @Override
    public String getEmail(){
        return email;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", grossSalary=" + grossSalary +
                '}';
    }
}
