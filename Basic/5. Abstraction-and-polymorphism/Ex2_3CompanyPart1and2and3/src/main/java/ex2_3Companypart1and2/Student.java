package ex2_3Companypart1and2;

public class Student extends Employee implements NetSalary {

    public Student(String name, double grossSalary, String email) {
        super(name, grossSalary, email);
    }

    @Override
    public double calculateNetSalary() {
        return getGrossSalary() - (getGrossSalary() * studentTax);
    }

    @Override
    public String toString() {
        return super.toString() + " " + "netSalary = " + calculateNetSalary();
    }
}
