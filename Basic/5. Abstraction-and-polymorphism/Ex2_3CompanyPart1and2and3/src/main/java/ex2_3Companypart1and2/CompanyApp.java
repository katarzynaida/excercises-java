package ex2_3Companypart1and2;

import java.util.Arrays;

public class CompanyApp {

    public static void main(String[] args) {

        Employee e1 = new FullTimeEmployee("Magda", 5000, "magda@wp.pl");
        Employee e2 = new Student("Michał", 2100, "michal@wp.pl");
        Employee e3 = new TemporaryEmployee("Piotrek", 7000, "piotrek@wp.pl");

        Company company = new Company(10, 15);

        System.out.println(company.getEmplyeesNumber());

        company.addEmployee(e1);
        company.addEmployee(e2);
        company.addEmployee(e3);


        System.out.println(company.getEmplyeesNumber());

        System.out.println(Arrays.toString(company.getAllEmployees()));

        System.out.println("jjjjjjjjjjjjjjjjjjjjjjjj");
        company.removeEmployee(e2);

        System.out.println(Arrays.toString(company.getAllEmployees()));

        System.out.println("kkkkkkkkkkkkkkkkkkkkkk");

        AccountingDepartment accountingDepartment = new AccountingDepartment();
        accountingDepartment.sendPaychecks(company);

        System.out.println("*******************");

        EventsDepartment eventsDepartment = new EventsDepartment();
        eventsDepartment.sendInvitationToTheParty("super party", company);

    }
}
