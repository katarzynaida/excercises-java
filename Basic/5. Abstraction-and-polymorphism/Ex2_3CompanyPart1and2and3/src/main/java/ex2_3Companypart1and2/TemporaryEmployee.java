package ex2_3Companypart1and2;

public class TemporaryEmployee extends Employee implements NetSalary {

    public TemporaryEmployee(String name, double grossSalary, String email) {
        super(name, grossSalary, email);
    }

    @Override
    public double calculateNetSalary() {
        return getGrossSalary() - (getGrossSalary() * temporaryEmployeeTax);
    }

    @Override
    public String toString() {
        return super.toString() + " " + "netSalary = " + calculateNetSalary();
    }
}
