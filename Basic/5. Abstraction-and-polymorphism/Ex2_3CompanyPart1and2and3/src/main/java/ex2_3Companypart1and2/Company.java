package ex2_3Companypart1and2;

public class Company implements CompanyEmployees, CompanyPeople{

    private Employee[] employees;
    private Customer[] customers;
    private Person[] people;
    private int emplyeesNumber;
    private int customersNumber;
    private int peopleNumber;

    public Company(int maxEmployees, int maxCystomers) {
        employees = new Employee[maxEmployees];
        customers = new Customer[maxCystomers];
        people = new Person[maxCystomers + maxEmployees];
        emplyeesNumber = 0;
        customersNumber = 0;
        peopleNumber = 0;
    }

    // dodaje obiekty klasy Employee
    public void addEmployee(Employee employee){
        if(employee == null){
            return;
        }
        if(emplyeesNumber < employees.length) {
            employees[emplyeesNumber++] = employee;
            people[peopleNumber++] = employee;
        }else{
            System.out.println("Brak miejsc");
        }
    }

    //DODAJE CUSTOMRA do tablicy
    public void addCustomer(Customer customer){
        if(customer == null){
            return;
        }
        if(customersNumber < customers.length){
            customers[customersNumber++] = customer;
            people[peopleNumber++] = customer;
        }else {
            System.out.println("Brak miejsc");
        }
    }

    // usuwa obiekty klasy Employee
    public void removeEmployee(Employee employee){
        if(employee == null){
            return;
        }
        int index = 0;
        while (index < employees.length){
            if(employee.equals(employees[index])){
                System.arraycopy(employees, index + 1, employees, index, employees.length - index - 1);
                emplyeesNumber--;
                break;
            } else{
                index++;
            }
        }
    }
    @Override
    public Employee[] getAllEmployees(){
        Employee[] result = new Employee[emplyeesNumber];
        System.arraycopy(employees, 0, result, 0, emplyeesNumber);
        return result;
    }
    @Override
    public Person[] getAllPeople(){
        Person[] result = new Person[peopleNumber];
        System.arraycopy(people, 0, result, 0, peopleNumber);
        return result;
    }


    // pobiera obiekty klasy Employee
    public Employee getEmployee(int index){
        if(index < 0 || index >= emplyeesNumber){
            System.out.println("Zbyt duży lub zbyt mały index");
        }
        return employees[index];
    }

    public Employee[] getEmployees() {
        return employees;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public int getEmplyeesNumber(){
        return emplyeesNumber;
    }

    public int getCustomersNumber(){
        return customersNumber;
    }


    public double sumOfNetSalaries(){
        int sum = 0;
        for(Employee employee: employees){
            sum += employee.calculateNetSalary();
        }
        return sum;
    }
    public double averageOfNetSalaries(){
        return sumOfNetSalaries() / emplyeesNumber;
    }


}
