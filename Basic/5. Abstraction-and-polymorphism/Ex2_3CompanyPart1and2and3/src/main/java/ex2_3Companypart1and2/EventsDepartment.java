package ex2_3Companypart1and2;

public class EventsDepartment {

    public void sendInvitationToTheParty(String partyName, CompanyPeople company){
        for(Person person: company.getAllPeople()){
            System.out.println(person.getEmail() + " " + partyName);
        }
    }
}
