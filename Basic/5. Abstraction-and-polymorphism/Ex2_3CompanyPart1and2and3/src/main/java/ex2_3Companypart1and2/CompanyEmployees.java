package ex2_3Companypart1and2;

public interface CompanyEmployees {
    Employee[] getAllEmployees();

    Employee getEmployee(int index);
}
