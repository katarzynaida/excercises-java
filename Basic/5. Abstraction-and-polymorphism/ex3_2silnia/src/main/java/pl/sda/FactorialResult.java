package pl.sda;

public class FactorialResult {
    private Integer result;
    private String errorMsg;

    public FactorialResult(Integer result, String errorMsg) {
        this.result = result;
        this.errorMsg = errorMsg;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
