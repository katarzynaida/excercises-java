package pl.sda;

import java.util.Scanner;

public class Factorial {
    // warunek ? .....(true) : .... (false) - operator trójargumentowy
    public static void main(String[] args) {
        System.out.println("Podaj liczbę całkowitą, dla której program policzy silnię.");
        Scanner scanner = new Scanner(System.in);
        int numberToFactorial = scanner.nextInt();
        FactorialResult factorialResult = factorial(numberToFactorial);
        System.out.println(factorialResult.getResult() == null ? factorialResult.getErrorMsg() : factorialResult.getErrorMsg() + factorialResult.getResult());
    }

    public static FactorialResult factorial(int numberToFactorial){
        if (numberToFactorial == 1 || numberToFactorial == 0){
            return new FactorialResult(1, "Twój wynik wynosi: ");
        }
        if(numberToFactorial < 0){
            return new FactorialResult(null, "Podałeś liczbę mniejszą od zera. Program liczby silnię dla liczb >= 0");
        }
        int result = 1;
        if(numberToFactorial > 1) {
            result = numberToFactorial * factorial(numberToFactorial - 1).getResult();
        }
        return new FactorialResult(result, "Twój wynik wynosi: ");
    }
}
