public interface Operation {
    double calculate();
}
