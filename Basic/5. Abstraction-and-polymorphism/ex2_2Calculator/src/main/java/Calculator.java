import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        /*
        Zaprojektuj prosty kalkulator liczb całkowitych. Będziesz potrzebować do tego:
        - interfejs Operation, który będzie zawierał bezargumentową metodę double calculate()
        - implementacje interfejsu Operation dla działań dodawania, odejmowania, mnożenia, dzielenia
        Każda implementacja interfejsu Operation powinna mieć konstruktor, który przyjmuje dwie liczby
        całkowite int (argumenty działania).
        Możesz rozszerzyć zadanie o przyjmowanie argumentów od użytkownika (Scanner)
         */

        Scanner scanner = new Scanner(System.in);
        System.out.println("To jest prosty kalkulator. Wprowadź pierwszą liczbę");
        int number1 = scanner.nextInt();
        System.out.println("Wprowadź znak działania jakie chcesz wykonać. \"+\" - dodawanie, \"-\" - odejmowanie, " +
                "\"*\" - mnożenie, \"/\" - dzielenie");
        String sign = scanner.next();
        System.out.println("Wprowadź drugą liczbę");
        int number2 = scanner.nextInt();

        Operation myOperation = createOperation(sign, number1, number2);
        System.out.println("Twój wynik to: " + myOperation.calculate());

    }

    public static Operation createOperation(String sign, int number1, int number2){
        Operation operation = new Addition(0, 0);
        switch (sign) {
            case "+":
                operation = new Addition(number1, number2);
                break;
            case "-":
                operation = new Subtraction(number1, number2);
                break;
            case "*":
                operation = new Multiplication(number1, number2);
                break;
            case "/":
                operation = new Division(number1, number2);
                break;
            default:
                System.out.println("Wprowadziłeś zły znak lub liczbę");
        }
        return operation;
    }
}
