package ex1_3university;

public class University {
    public static void main(String[] args) {
        /*
        Utwórz program składający się z dwóch klas:
        Student - pozwala przechowywać informację o imieniu, nazwisku i numerze indeksu studenta
        University - klasa z metodą main, w której utworzysz kilku (co najmniej 2 studentów)
        W klasie Student zdefiniuj zmienną statyczną, która wraz z tworzeniem nowego obiektu tej
        klasy będzie zwiększana, a tym samym pozwoli śledzić ilość studentów na uczelni.
        Po utworzeniu co najmniej dwóch studentów w klasie University, wyświetl liczbę studentów
        na uczelni.
         */

        Student student1 = new Student("Paulina", "Kwiatkowska", 54821);
        Student student2 = new Student("Marek", "Zieliński", 65421);
        System.out.println(Student.numberOfStudents);
    }
}
