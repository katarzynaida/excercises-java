package ex1_3university;

public class Student {
    private String firstName;
    private String lastName;
    private int numberOfIndex;
    public static int numberOfStudents;


    public Student(String firstName, String lastName, int numberOfIndex) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.numberOfIndex = numberOfIndex;
        numberOfStudents++;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNumberOfIndex() {
        return numberOfIndex;
    }

    public void setNumberOfIndex(int numberOfIndex) {
        this.numberOfIndex = numberOfIndex;
    }
}
