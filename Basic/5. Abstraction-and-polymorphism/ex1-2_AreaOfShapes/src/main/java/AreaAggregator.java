import java.util.Arrays;

public class AreaAggregator {
    public static void main(String[] args) {

        Shape rectangle = new Rectangle(20,40);
        rectangle.area();

        Shape square = new Square(20);
        square.area();

        Shape circle = new Circle(5);
        circle.area();

        Shape[] shapes = {rectangle, square, circle};

        double areaSum = aggregate(shapes);

        System.out.println(areaSum);
    }
    public static double aggregate(Shape[] shapes){
        double sum = 0;
        for(int i = 0; i < shapes.length; i++){
            sum = sum + shapes[i].area();
        }
        return sum;
    }
}

