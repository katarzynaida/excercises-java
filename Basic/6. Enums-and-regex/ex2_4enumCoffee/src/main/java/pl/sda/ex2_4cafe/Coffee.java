package pl.sda.ex2_4cafe;

public enum Coffee {

    LATTE (true, true, true, true),
    AMERICANO (true, false, false, true),
    MACCHIATO (true, false, true, false);

    boolean milk;
    boolean whippedcream;
    boolean honey;
    boolean nuts;

    Coffee(boolean milk, boolean whippedcream, boolean honey, boolean nuts) {
        this.milk = milk;
        this.whippedcream = whippedcream;
        this.honey = honey;
        this.nuts = nuts;
    }

    @Override
    public String toString() {
        String result = this.name() + " (";

        if(milk) {
            result += "mleko, ";
        }
        if(whippedcream){
            result += "bita śmietana, ";
        }
        if(honey){
            result += "miód, ";
        }
        if(nuts){
            result += "orzechy)";
        }
        return result;
    }
}
