package pl.sda.ex2_4cafe;

import java.util.Arrays;
import java.util.Scanner;

public class CoffeeDemo {

    public static void main(String[] args) {

        System.out.println("Oto kawy do wyboru: " + Arrays.toString(Coffee.values()));

        System.out.println("Wybierz kawę");

        Scanner scanner = new Scanner(System.in);

        String coffeeChoosen = scanner.nextLine();

        System.out.println("Wybrałeś kawę: " + coffeeChoosen);
    }
}
