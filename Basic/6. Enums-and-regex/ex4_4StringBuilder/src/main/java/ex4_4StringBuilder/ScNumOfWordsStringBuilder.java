package ex4_4StringBuilder;

import java.util.Arrays;
import java.util.Scanner;

public class ScNumOfWordsStringBuilder {
    public static void main(String[] args) {

        /*
        Pobierz od użytkownika liczbę, która określać będzie ile wyrazów użytkownik chce wprowadzić.
        Następnie wczytaj od niego listę ciągów znaków na podstawie, których wygenerujesz wynik.
        Wynikiem jest wyraz składający się z ostatnich liter każdego z wprowadzonych słów. Wykorzystaj
        klasy String i StringBuilder.
         */

        System.out.println("Podaj liczbę, która określi ile wyrazów chcesz wprowadzić.");
        Scanner scanner = new Scanner(System.in);
        int numberOfWords = scanner.nextInt();
        scanner.nextLine();
        String[] wordsFromPeron = new String[numberOfWords];

        addToArray(numberOfWords, wordsFromPeron, scanner);
        System.out.println("Poniżej wyświetlone zostaną wszystkie słowa, które wprowadziłeś");
        System.out.println(Arrays.toString(wordsFromPeron));

        System.out.println("Poniżej wyświetlone zostanie słowo utworzone z ostatnich litech słów, które wprowadziłeś.");
        String wordFromLastLettersOfWords = lastLettersFrom(wordsFromPeron);
        System.out.println(wordFromLastLettersOfWords);

    }

    public static void addToArray(int numberOfWords, String[] wordsFromPerson, Scanner scanner){
        for(int i = 0; i < numberOfWords; i++){
            System.out.println("Wprowadz kolejny wyraz:");
            wordsFromPerson[i] = scanner.nextLine();
        }
    }

    public static String lastLettersFrom(String[] words) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < words.length; i++){
            String oneWord = words[i];
            int lastLetterPosition = oneWord.length() - 1;
            char lastLetter = oneWord.charAt(lastLetterPosition);
            stringBuilder.append(lastLetter);
        }
        return stringBuilder.toString();
    }
}
