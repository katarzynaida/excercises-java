package ex5_4IP;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Ex5_4IP {
    public static void main(String[] args) {
        /*
        Napisać program wczytujący z konsoli dane. Sprawdzić czy podane dane to poprawny adres IP
        o formacie {a.b.c.d} gdzie a,b,c,d mogą przyjmować wartości od 0 – 255 (mogą zawierać od
        jednego do trzech znaków). Zwróć użytkownikowi informację czy wprowadził prawidłowy adres.
         */
        boolean validIP;

        do {
            Scanner scanner = new Scanner(System.in);

            System.out.println("Wprowadź adres IP: ");
            String ip = scanner.nextLine();

            validIP = validateIP(ip);
            if (!validIP) {
                System.out.println("Błędne IP");
            }else{
                System.out.println("Wprowadzony adres IP jest poprawny");
            }
        }
        while (!validIP);
    }

    public static boolean validateIP(String ip){
        Pattern pattern1 = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
        return pattern1.matcher(ip).matches();
    }
}
