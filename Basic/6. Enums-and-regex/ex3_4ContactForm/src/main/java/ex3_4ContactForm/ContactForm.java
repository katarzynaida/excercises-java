package ex3_4ContactForm;

import java.util.Scanner;
import java.util.regex.Pattern;

public class ContactForm {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        boolean validNameResult;
        boolean validSurnameResult;
        boolean validEmailResult;
        boolean validPasswordResult;

        do {
            System.out.println("Wprowadź imię: ");
            String name = scanner.nextLine();

            validNameResult = validateName(name);
            if(!validNameResult) {
                System.out.println("Wprowadziłeś niedozwolony znak lub za krótkie słowo itp.");
            }
        }
        while(!validNameResult);

        do {
            System.out.println("Wprowadź nazwisko: ");
            String surname = scanner.nextLine();

            validSurnameResult = validateSurname(surname);
            if (!validSurnameResult) {
                System.out.println("Wprowadziłeś niedozwolony znak lub za krótkie słowo itp.");
            }
        }
        while (!validSurnameResult);


        do {
            System.out.println("Wprowadź email: ");
            String email = scanner.nextLine();

            validEmailResult = validateEmail(email);
            if (!validEmailResult) {
                System.out.println("Coś poszło nie tak. Pamiętaj, że e-mail musi zawierać znak \"@\", a także kończyć się \".pl\" lub \".com\"");
            }
        }
        while(!validEmailResult);

        do {
            System.out.println("Wprowadź hasło: ");
            String password = scanner.nextLine();

            validPasswordResult = validatePassword(password);
            if (!validPasswordResult) {
                System.out.println("Coś poszło nie tak. Pamiętaj, że hasło musi mieć minimum 3 znaki oraz musi posiadać jeden z nastpujących znaków @#$, w środku hasła.");
            }
        }
        while (!validPasswordResult);

        System.out.println("Dziękujemy za rejestrację :)");
    }

    public static boolean validateName(String name) {
        /*
        1. Pierwsza litera musi być wielka z zakresu A-Z i może wystąpić tylko raz
        2. Kolejne litery mają być małe z zakresu a-z
        3. W sumie użytkownik musi wprowadzić minimum 3 znaki
         */

        Pattern pattern1 = Pattern.compile("[A-Z]{1}[a-z]{2,}");
        return pattern1.matcher(name).matches();
    }

    public static boolean validateSurname(String surname){
        /*
        1. Musi zaczynać się od wielkiej litery A-Z
        2. Kolejne litery mają być małe z zakresu a-z
        3. Jest możliwość wprowadzania nazwisk dwuczłonowych – nowy człon zaczyna się
        wielką literą A-Z i jest poprzedzony pauzą „-„ np. „Kwasniewska-Dobosz”
        4. W sumie użytkownik musi wprowadzić minimum 3 znaki
         */

        Pattern pattern2 = Pattern.compile("[A-ZĄĆŁŃÓŚŻŹ][a-ząćłńóśźż]{2,}(-[A-ZĄĆŁŃÓŚŻŹ][a-ząćłńóśźż])?");
        return pattern2.matcher(surname).matches();
    }

    public static boolean validateEmail(String email){
        /*
        1. Musi zawierać znak @
        2. Ostatnie człon nazwy to „.pl” lub „.com”
         */

        Pattern pattern3 = Pattern.compile(".+@{1}.+(\\.pl|\\.com){1}");
        return pattern3.matcher(email).matches();
    }

    public static boolean validatePassword(String password) {
        /*
        1. Musi zawierać minimum 3 znaki
        2. Musi zawierać co najmniej jeden znak specjalny z zakresu (@#$)
         */

        Pattern pattern4 = Pattern.compile(".+[@#$]+.+");
        return pattern4.matcher(password).matches();
    }
}
