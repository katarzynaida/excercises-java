package ex1_4enumColor;

import java.util.Scanner;

public class ColorApp {
    public static void main(String[] args) {
        /*
        Stwórz enum o nazwie Color, który będzie przechowywał podstawowe kolory wraz z informacją czy
        dany kolor jest ładny czy nie. (Decyzja o tym, który kolor jest ładny, a który nie pozostawiam wam)
        W klasie ColorApp pobierz od użytkownika (Scanner) nazwę koloru, a następnie wyświetl informację
        o tym czy wybrał ładny czy brzydki kolor.
         */

        System.out.println("Dostępne kolory: ");
        for(Color color: Color.values()){
            System.out.println(color);
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj kolor: ");
        String nameOfColor = scanner.nextLine();

        System.out.println("Twój kolor to: " + nameOfColor + " . To jest " + Color.valueOf(scanner.nextLine()).getDescription() + " kolor.");

    }
}
