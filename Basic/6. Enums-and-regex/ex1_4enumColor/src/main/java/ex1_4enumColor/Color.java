package ex1_4enumColor;

public enum Color {
    ZIELONY ("ładny"),
    NIEBIESKI ("ładny"),
    ŻÓŁTY ("brzydki"),
    CZERWONY ("ładny"),
    CZARNY ("brzydki");

    private String description;


    Color(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
