import java.util.Arrays;
import java.util.Scanner;

public class Exercise8 {
    public static void main(String[] args) {
        /*
            1. Napisz program symulujące grę w lotto.
            2. Użytkownik podaj 6 różnych liczb [1-49].
            3. Program losuje 6 różnych liczb.
            4. Program wyświetla, jakie liczby zostały wylosowane oraz ile zgadł użytkownik.
        */
        Scanner scanner = new Scanner(System.in);
        System.out.println("LOTTO \n Będziesz musiał podać 6 liczb.");

        int[] amountOfNumbers = new int[6];

        for (int i = 0; i < amountOfNumbers.length; i++) {
            System.out.println("Podaj liczbę z zakresu od 1 do 49");
            int num = scanner.nextInt();
            if(num < 50 && num > 0 && isNotAlreadyGiven(num, amountOfNumbers, i)) {
                amountOfNumbers[i] = num;
            } else {
               System.out.println("Liczba nie mieści się w przedziale od 1 do 49 albo podałeś już tę liczbę wcześniej");
               i--;
            }
        }

        for(int i = 0; i < amountOfNumbers.length; i++) {
            boolean swap = false;

            for (int j = 0; j < amountOfNumbers.length - 1; j++) {
                if (amountOfNumbers[j] > amountOfNumbers[j + 1]) {
                    int temp = amountOfNumbers[j];
                    amountOfNumbers[j] = amountOfNumbers[j + 1];
                    amountOfNumbers[j + 1] = temp;
                    swap = true;
                }
                if (swap = false){
                    break;
                }
            }
        }
        System.out.println("Twoje liczby to: " + Arrays.toString(amountOfNumbers));

        int[] drawnNumbers = new int[6];
        for (int i = 0; i < drawnNumbers.length; i++) {
            drawnNumbers[i] = (int)(Math.random()*49) + 1 ;

            for (int j = 0; j < i; j++) {
                if (drawnNumbers[i] == drawnNumbers[j]) {
                    i--;
                    break;
                }
            }
        }

        for(int i = 0; i < drawnNumbers.length; i++) {
            boolean swap = false;

            for (int j = 0; j < drawnNumbers.length - 1; j++) {
                if (drawnNumbers[j] > drawnNumbers[j + 1]) {
                    int temp = drawnNumbers[j];
                    drawnNumbers[j] = drawnNumbers[j + 1];
                    drawnNumbers[j + 1] = temp;
                    swap = true;
                }
                if (swap = false){
                    break;
                }
            }
        }
        System.out.println("Wylosowane liczby w lotto to: " + Arrays.toString(drawnNumbers));

        int counter = 0;
        for(int i = 0; i < amountOfNumbers.length; i++){
            for(int j = 0; j < amountOfNumbers.length; j++){
                if(amountOfNumbers[i] == drawnNumbers[j]){
                    counter++;
                }
            }
        }
        System.out.println("Trafiłeś " + counter + " liczb w lotto");


    }
    // curentCount = i (w tamtej metodzie wyżej, i stamtąd wzrasta)
    private static boolean isNotAlreadyGiven(int num, int[] amountOfNumbers, int currentCount) {
        for(int z = 0; z < currentCount; z++) {
            if(num == amountOfNumbers[z]) {
                return false;
            }
        }
        return true;
    }
}
