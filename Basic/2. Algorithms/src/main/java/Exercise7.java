import java.util.Scanner;

public class Exercise7 {
    public static void main(String[] args) {
        /* Napisz prosty kalkulator dla liczb zmiennoprzecinkowych, który pozwala użytkownikowi kolejno na:
             Wprowadzenie pierwszej liczby
             Wprowadzenie jednego z podstawowych działań matematycznych (plus, minus, podziel, pomnóż) -
            w postaci znaków +, -, /, *
             Wprowadzenie drugiej liczby
            Program powinien działać cały czas do momentu, aż użytkownik wybierze inny znak spośród
            wymienionych.
        */
            boolean end = true;
        do {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Wprowadź pierwszą liczbę: ");
            double firstNumber = scanner.nextDouble();
            scanner.nextLine();

            System.out.println("Wprowadź jeden z następujących znaków: \n + wykona dodawanie \n " +
                    "- wykona odejmowanie \n * wykona mnożenie \n / wykona dzielenie ");
            String sign = scanner.nextLine();

            System.out.println("Wprowadź drugą liczbę");
            double secondNumber = scanner.nextInt();

            double result = 0;
            switch (sign){
                case "+":
                    result = firstNumber + secondNumber;
                    System.out.println("Wynik działania wynosi: " + result);
                    break;
                case "-":
                    result = firstNumber - secondNumber;
                    System.out.println("Wynik działania wynosi: " + result);
                    break;
                case "*":
                    result = firstNumber * secondNumber;
                    System.out.println("Wynik działania wynosi: " + result);
                    break;
                case "/":
                    if(secondNumber == 0){
                        System.out.println("Pamiętaj cholero nie dziel przez 0 ;)");
                        break;
                    }
                    result = firstNumber / secondNumber;
                    System.out.println("Wynik działania wynosi: " + result);
                    break;
                default:
                    end = false;
                    System.out.println("Wprowadzono inny znak.");
            }
        }
        while (end);

        System.out.println("Wynik wszystkich działań wynosi: ");
    }
}
