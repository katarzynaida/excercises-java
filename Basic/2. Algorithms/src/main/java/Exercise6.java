import java.util.Scanner;

public class Exercise6 {
    public static void main(String[] args) {
        /*
        Napisz program, który najpierw pobiera od użytkownika liczbę mówiącą o tym ile liczb użytkownik
        chce wprowadzić. Następnie pobierz od niego tyle liczb ile zadeklarował, zsumuj je i wyświetl na
        ekranie.
         */

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ile liczb chcesz wprowadzić? (Podaj liczbę)");
        int amountOfNumbers = scanner.nextInt();

        int sum = 0;

        for(int i = 0; i < amountOfNumbers; i++){
            System.out.println("Wprowadź liczbę");
            int nextNumber = scanner.nextInt();
            sum = sum + nextNumber;
        }
        System.out.println("Suma wprowadzonych liczb wynosi: " + sum);
    }
}
