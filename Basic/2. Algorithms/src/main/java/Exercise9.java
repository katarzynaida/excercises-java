import java.util.Scanner;

public class Exercise9 {
    public static void main(String[] args) {
        /*
        Napisz program, który prosi użytkownika o podanie dwóch liczb,
        a następnie liczy NWD podanych liczb.
         */
        System.out.println("Program ten policzy największy wspólny dzielnik dla " +
                "dwóch podanych przez Ciebie liczb. \n Podaj pierwszą z liczb.");
        Scanner scanner = new Scanner(System.in);
        int num1 = scanner.nextInt();
        System.out.println("Podaj drugą liczbę.");
        int num2 = scanner.nextInt();
        System.out.println("NWD liczb: " + num1 + " i " + num2 + " wynosi " + nwd(num1, num2));
    }

    public static int nwd(int num1, int num2){
        while(num1 != num2){
            if (num1 > num2){
                num1 = num1 - num2;
            }else{
                num2 = num2 - num1;
            }
        }
        return num1;
    }
}
