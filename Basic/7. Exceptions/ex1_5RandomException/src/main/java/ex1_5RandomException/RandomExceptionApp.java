package ex1_5RandomException;

import java.util.Random;

public class RandomExceptionApp {
    /*
    Napisz 3 nowe wyjątki, gdzie utworzysz pole o nazwie errorCode oraz przesłonisz konstruktor z
    klasy MyFirstException przyjmujący dowolny komunikat(String message) oraz kod błędu (int errorCode).
    W klasie RandomExceptionApp utwórz metodę generateException(), która za każdym wywołaniem będzie
    rzucała jeden z trzech utworzonych wyjątków.
    W metodzie main opakuj wywołanie powyższej metody blokiem try-catch, zawierającym po jednym bloku
    catch dla każdego z wyjątków. W każdym bloku catch zwróć wiadomość oraz kod błędu z danego wyjątku.
     */

    public static void main(String[] args) {
        RandomExceptionApp randomExceptionApp = new RandomExceptionApp();

        try{
            randomExceptionApp.generateException();
        } catch(MyFirstException first){
            System.out.println(first.getMessage() + " kod błędu: " + first.getErrorCode());
        } catch (MySecondException second){
            System.out.println(second.getMessage() + " kod błędu: " + second.getErrorCode());
        } catch (MyThirdException third){
            System.out.println(third.getMessage() + " kod błędu: " + third.getErrorCode());
        }
    }

    private void generateException() throws MyFirstException, MySecondException, MyThirdException{
        Random random = new Random();
        int randomNumber = random.nextInt(3);

        switch(randomNumber){
            case 0:
                throw new MyFirstException("message 1", 5478);
            case 1:
                throw new MySecondException("message 2", 54777);
            case 2:
                throw new MyThirdException("message 3", 55555);
        }
    }
}
