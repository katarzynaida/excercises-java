package ex1_5RandomException;

public class MySecondException extends Exception{

    private int errorCode;

    public MySecondException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
