package ex1_5RandomException;

public class MyFirstException extends Exception{

    private int errorCode;

    public MyFirstException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
