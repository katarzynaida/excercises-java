package ex1_5RandomException;

public class MyThirdException extends Exception{

    private int errorCode;

    public MyThirdException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
