package ex2_5CalculatorException.calculator;

import exception.UnknownMarkException;
import operation.*;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {
        /*
        Dot. Zadanie 2 z „zadania_2.pdf” – kalkulator
        Stwórz program odporny na wpisywanie błędnych danych, na podstawie utworzonego już kalkulatora.
        W przypadku dzielenia przez 0 zostanie wygenerowany wyjątek ArithmticException, który należy obsłużyć
        i wyświetlić odpowiedni komunikat użytkownikowi.
        W przypadku wprowadzenia błędnego typu do metody scanner.next….(), zostanie wygenerowany wyjątek
        InputMismatchException – również i jego należy obsłużyć i wysłać komunikat.
        Należy utworzyć również nowy wyjątek UnknownMarkException. Będzie on rzucany w momencie, gdy użytkownik
        wprowadzi błędny znak.
         */
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("To jest prosty kalkulator. Wprowadź pierwszą liczbę");
            int number1 = scanner.nextInt();
            System.out.println("Wprowadź znak działania jakie chcesz wykonać. \"+\" - dodawanie, \"-\" - odejmowanie, " +
                    "\"*\" - mnożenie, \"/\" - dzielenie");
            String sign = scanner.next();
            validateSign(sign);

            System.out.println("Wprowadź drugą liczbę");
            int number2 = scanner.nextInt();

            Operation myOperation = createOperation(sign, number1, number2);
            System.out.println("Twój wynik to: " + myOperation.calculate());

        } catch(UnknownMarkException a) {
            System.out.println(a.getMessage());
        }catch(ArithmeticException a) {
            System.out.println("Pamiętaj cholero, nie dziel przez 0!");
        }catch(InputMismatchException a) {
            System.out.println("Wprowadziłęś błędny znak. Musisz wprowadzić liczbę.");
        }
    }

    public static void validateSign(String sign) throws UnknownMarkException{
        if (!sign.equals("+") && !sign.equals("-") && !sign.equals("*") && !sign.equals("/")) {
            throw new UnknownMarkException("Wprowadziłeś zły znak operacji.");
        }
    }

    public static Operation createOperation(String sign, int number1, int number2) throws UnknownMarkException {
        Operation operation;
        switch (sign) {
            case "+":
                operation = new Addition(number1, number2);
                break;
            case "-":
                operation = new Subtraction(number1, number2);
                break;
            case "*":
                operation = new Multiplication(number1, number2);
                break;
            case "/":
                operation = new Division(number1, number2);
                break;
            default:
                throw new UnknownMarkException("Wprowadziłeś zły znak operacji.");
        }
        return operation;
    }
}
