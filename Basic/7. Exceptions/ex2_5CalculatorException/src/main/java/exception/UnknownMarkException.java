package exception;

public class UnknownMarkException extends Exception {

    public UnknownMarkException(String message) {
        super(message);
    }
}
