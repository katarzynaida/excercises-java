package operation;

public interface Operation {
    double calculate();
}
