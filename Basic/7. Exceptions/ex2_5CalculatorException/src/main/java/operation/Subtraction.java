package operation;

public class Subtraction implements Operation{
    private int number1;
    private int number2;

    public Subtraction(int number1, int number2) {
        this.number1 = number1;
        this.number2 = number2;
    }

    public double calculate() {
        return number1 - number2;
    }
}
